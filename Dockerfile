FROM node:current-alpine

WORKDIR /opt/myapp
COPY ./ .
EXPOSE 3000

RUN npm install
CMD ["node", "src/app.js"]

