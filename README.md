# k3d-example

This is an example project that uses [k3d](k3d.io) to run a local kubernetes cluster to host a Hello World express app. It is recommended to familiarize yourself with basic kubernetes concepts before
diving into this example.

## Quickstart

Assuming you already have kubectl, helm, and k3d installed, run the following commands to startup a local cluster and deploy rancher. Otherwise, see [Installing kubectl, helm, and k3d]()

```shell
make
```

Rancher takes a while (a few minutes) to start but will become available at [https://localhost:8001/](https://localhost:8001/). The Makefile starts k3d with a container registry that is addressed at
`k3d-mycluster-registry.localhost`. In order to push images to the repository add a record to your hosts file. 

On mac/linux, add a line `127.0.0.1 k3d-mycluster-registry.localhost` to `/etc/hosts`. Once the registry address is resolvable, you can build and push the docker image via the `make app-image` command and deploy
the app manifest the k3d cluster.

```shell
make app-image
kubectl apply -f manifests/app/myapp.yaml 
```

The express app will be available at [http://localhost:8080/myapp/](http://localhost:8080/myapp/) . The app is exposed via an ingress rule using the default ingress controller (traefik).

## Cleanup

To delete everything (except for kubectl, helm, and k3d) run 
```shell
make clean
```

## Installing kubectl, helm, and k3d

To install required tools, run the following commands. (Note the TAG env var in the k3d install. This specific version of k3d works for this example, others may not)
```shell
curl -LO "https://dl.k8s.io/release/$$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
curl -s https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | TAG=v5.4.6 bash
``` 

## Optional

### Viewing the traefik dashboard

Traefik is the default ingress controller for the k3d cluster. To view the traefik dashboard, you can forward a local port to the port associated with the traefik dashboard pod.
```shell
kubectl port-forward -n kube-system $$(kubectl get pods -n kube-system| grep '^traefik-' | awk '{print $1}') 9000:9000
```

### Deploying a local Argo

```shell
make argo
argocd admin initial-password -n argocd
kubectl port-forward svc/argocd-server -n argocd 8080:443
```

### Creating a localhost TLS certificate and configuring Argo with self signed cert 

Create a new key pair and create a secret in kubernetes for argo to reference. Argo will look for TLS configuration on a schedule and hot reloud if it's available.

```shell
openssl req -x509 -out localhost.crt -keyout localhost.key -newkey rsa:2048 -nodes -sha256 -subj '/CN=localhost' -extensions EXT -config openssl.config
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain localhost.crt
kubectl create secret -n argocd tls argocd-server-tls \
    --cert=localhost.crt \
    --key=localhost.key
```

## Relevant Readings
- [Kubernetes concepts](https://kubernetes.io/docs/concepts/)
- [Accessing a local k3d registry](https://k3d.io/v5.2.1/usage/registries/#using-a-local-registry)
- [Rancher Getting Started](https://rancher.github.io/dashboard/getting-started/concepts)
- [Medium article How-To used as a basis for this repo](https://medium.com/47billion/playing-with-kubernetes-using-k3d-and-rancher-78126d341d23)
- [K3d and traefik](https://k3d.io/v5.4.9/usage/k3s/#traefik) 
