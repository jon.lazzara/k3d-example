const express = require('express')
const app = express()
const port = 3000

app.get('/myapp', (req, res) => {
  console.log('Received request at ', new Date());
  res.send('Hello World!');
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})

