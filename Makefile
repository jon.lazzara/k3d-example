cluster: registry_port = 50555
app-image: version = 1.0

run: cluster rancher

cluster:
	k3d registry create mycluster-registry.localhost --port $(registry_port)
	k3d cluster create mycluster -p "8080:80@loadbalancer" -p "8000:30080@agent:0" -p "8001:30081@agent:0" -p "8002:30082@agent:0" --agents 2 --registry-use k3d-mycluster-registry.localhost:$(registry_port)

.registryport:
	docker ps -f name=k3d-mycluster-registry --format '{{.Ports}}' | sed -e 's/.*:\(.*\)-.*/\1/' > .registryport
	if [[ -z $$(grep '[^[:space:]]' .registryport) ]] ; then echo "No port found"; rm .registryport; exit 1; fi

app-image: .registryport
	docker build -t k3d-mycluster-registry.localhost:$$(cat .registryport)/myapp:v$(version) .
	docker push k3d-mycluster-registry.localhost:$$(cat .registryport)/myapp:v$(version)

argo:
	kubectl create namespace argocd
	kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
	kubectl create configmap argocd-cmd-params-cm -n argocd --from-literal=server.insecure="true" --from-literal=server.rootpath="/argocd" --from-literal=server.basehref="/argocd" -o yaml --dry-run=client | kubectl apply -f -

rancher:
	helm install rancher rancher-latest/rancher \
       --namespace cattle-system \
       --create-namespace \
       --set ingress.enabled=false \
       --set tls=external \
       --set replicas=1
	kubectl apply -f manifests/rancher.yaml

rancher-key:
	kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}{{"\n"}}'

clean:
	k3d cluster delete mycluster
	k3d registry delete k3d-mycluster-registry.localhost
	rm .registryport

